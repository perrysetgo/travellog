$(document).ready(function() {

  var htmlString =
        '<div>' +
        '<form>' +
        'Add Friend' +
        '<input type="text" class="form-control add-friend-input">' +
        '<input type="checkbox" name="social" value="phone">Phone' +
        '<input type="checkbox" name="social" value="email">Email' +
        '<input type="checkbox" name="social" value="twitter">Twitter' +
        '<input type="checkbox" name="social" value="facebook">FaceBook' +
        '<input type="checkbox" name="social" value="linkedin">LinkedIn' +
        '<form>' +
        '</div>'

  $(".friend-form-div").delegate(".add-friend-input","click", function() {
    $(".friend-form-div").append(htmlString);
    $(".add-friend-input").off();
    $(".form-control").removeClass("add-friend-input");
    $(".form-control").last().addClass("add-friend-input")
  });


  var selected = [];
  $('#checkboxes input:checked').each(function() {
      selected.push($(this).attr('name'));
      console.log(selected); 
  });

//find a way to check
$('input[type="checkbox"]').click(function(){
              if($(this).is(":checked")){
                var type = $('input[type="checkbox"]').val();
                console.log(type);
                $(".friend-form-div").append('<label for="' + type + '">' + type +
                '<input type="text" id="'+ type +'" class="form-control">');

              }
              else if($(this).is(":not(:checked)")){
                  alert("Checkbox is unchecked.");
              }
          });

//http://jsfiddle.net/dvCmR/

// http://stackoverflow.com/questions/2155622/get-a-list-of-checked-checkboxes-in-a-div-using-jquery

$("form#new-location").submit(function(event) {
    event.preventDefault();

    var inputtedLocationName = $("input#location-name").val();
    var inputtedLocationState = $("input#location-state").val();
    var inputtedLocationCountry = $("input#location-country").val();
    var inputtedDateVisitedStart = $("input#date-visited-start").val();
    var inputtedDateVisitedEnd = $("input#date-visited-end").val();
    var inputtedNotes = $("textarea#location-notes").val();
    var inputtedFriends = $("input#add-friend").val();

    var newLocation = {
      locationName: inputtedLocationName,
      locationState: inputtedLocationState,
      locationCountry: inputtedLocationCountry,
      dateVisitedStart: inputtedDateVisitedStart,
      dateVisitedEnd: inputtedDateVisitedStart,
      locationNotes: inputtedNotes,
      };

      $("ul#locations").append("<li><span class='location'>" + newLocation.locationName + "</span></li>");

    $(".location").last().click(function() {
      $(".show-location").show();

      $(".show-location h2").text(newLocation.locationName);
      $(".location-name").text(newLocation.locationName);
      $(".location-state").text(newLocation.locationState);
      $(".location-country").text(newLocation.locationCountry);
      $(".date-visited-start").text(newLocation.dateVisitedStart);
      $(".date-visited-end").text(newLocation.dateVisitedEnd);
      $(".location-notes").text(newLocation.locationNotes);
      });

    $("input#location-name").val(""); //clear
    $("input#location-state").val("");
    $("input#location-country").val("");
    $("input#date-visited-start").val("");
    $("input#date-visited-end").val("");
    $("textarea#location-notes").val("");
  });
});
